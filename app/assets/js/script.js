document.addEventListener('DOMContentLoaded',function(){ //загрузка файлов, нужно сделать универсальной, чтобы в fetch передавался параметр
    //history.pushState(null,'',"/#/"+"comparison");
    fetch('html/menu.html')
        .then((resolve)=>{
            if(resolve.status != 200) throw new Error("Some problem with load menu");
            return resolve.text();
        })
        .then((text)=>{
            menu.innerHTML=text;
        })
        .catch(error=>menu.innerHTML=error)
    page('comparison');
})

function loadPage(event){
    event.preventDefault();
    page(event.target.name);
}

function page(name){
    history.pushState(null,'',"/#/"+name);
    fetch('html/menu.html')
        .then((resolve)=>{
            if(resolve.status != 200) throw new Error("Some problem with load menu");
            return resolve.text();
        })
        .then((text)=>{
            menu.innerHTML=text;
        })
        .catch(error=>menu.innerHTML=error)
    fetch("html/"+name + ".html")
        .then((resolve)=>{
            if(resolve.status != 200) throw new Error("Some problem with load page");
            return resolve.text();
        })
        .then((text)=>{
            container.innerHTML=text;
        })
        .then(()=>{
            if(name == 'comparison'){
                comparison();
            }
            else if(name == 'replacement'){
                replacement();
            }
        })
        .catch(error=>container.innerHTML=error)
}


