function replacement(){
    var block_cases = document.querySelector("#block-cases-replace"); //берутся все элементы в block-cases-replace
    
    /*for(var i = 0; i < block_cases.children.length;i++){
        console.log(block_cases.children.length);
        console.log(block_cases.children[i].children[0]);
        replaceSubstring(block_cases.children[i].children[0]);
    } */   
    regexp.oninput = function(event){ //если происходит событие в строке с регуляркой
        //regex = convert_to_regexp(event.target.value);
        for(var i = 0; i < block_cases.children.length;i++){
            replaceSubstring(block_cases.children[i].children[0],block_cases.children[i].children[1],block_cases.children[i].children[2]);
        }
    }
    replace_str.oninput = function(event){ //если происходит событие в строке с регуляркой
        //regex = convert_to_regexp(event.target.value);
        for(var i = 0; i < block_cases.children.length;i++){
            replaceSubstring(block_cases.children[i].children[0],block_cases.children[i].children[1],block_cases.children[i].children[2]);
        }
    }

    block_cases.oninput = function(event){ //необходимо сделать обход по всем элементам
        console.log(event);
        //replaceSubstring(event.target);   
    }

    addition_replace.onclick = function(event){    
        console.log(event);
        let caseBlock,input,answer,expextation,removeButton;

        var newExample = document.createElement("div");
        var attrClass = document.createAttribute("class");
        attrClass.value = "my-case-block addClass";
        newExample.setAttributeNode(attrClass);
        newExample.innerHTML= '<input class="input-for-replace" type="text" placeholder="Введите пример">'+
                    '<div class="button replace-block no-matched"><span>Result</span></div>'+
                    '<input class="input-for-compare" type="text" placeholder="Ожидаемый результат">'+
                    '<div type="button" class="button btn_remove">REMOVE</div>';
            newExample.children[3].onclick = function(event){
                newExample.classList.remove("addClass");
                newExample.classList.add("removeClass");
                newExample.addEventListener("webkitAnimationEnd", function()
                {
                    block_cases.removeChild( newExample );
                })
            }
        newExample.children[0].oninput = function(){
            replaceSubstring(newExample.children[0],newExample.children[1],newExample.children[2]);
        }
        newExample.children[2].oninput = function(){
            replaceSubstring(newExample.children[0],newExample.children[1],newExample.children[2]);
        }
        var block = document.getElementById("block-cases-replace");
        block.appendChild(newExample);
        for(var i = 0; i < block_cases.children.length;i++){
            replaceSubstring(block_cases.children[i].children[0],block_cases.children[i].children[1],block_cases.children[i].children[2]);
        }
    }
}

function replaceSubstring(input, resReplace, valForCompare){
    regex = convert_to_regexp(regexp.value);
    //символы для замены
    var replacing = replace_str.value;
    //console.log(input.nextElementSibling);
    var str = input.value;
    var result = str.replace(regex,replacing);
    //console.log(result);
    if(result){
        resReplace.innerHTML = result;
    }
    else{
        resReplace.innerHTML = '<span>Result</span>'
    }

    if(result && result===valForCompare.value){
        resReplace.classList.remove('no-matched');
        resReplace.classList.add('matched');
    }
    else{
        resReplace.classList.remove('matched');
        resReplace.classList.add('no-matched');
    }
}