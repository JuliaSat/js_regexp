function convert_to_regexp(value){
    return new RegExp(`^${value}$`,'');
}
var regex;
function comparison(){
    
    var block_cases = document.querySelector("#block-cases"); //берутся все элементы в block-cases

    /*for(var i = 0; i < block_cases.children.length;i++){
        console.log(block_cases.children.length);
        toggleMatch(block_cases.children[i].children[0]);
    }*/

    regexp.oninput = function(event){ //если происходит событие в строке с регуляркой
        //regex = convert_to_regexp(event.target.value);
        for(var i = 0; i < block_cases.children.length;i++){
            toggleMatch(block_cases.children[i].children[0]);
        }
    }

    block_cases.oninput = function(event){ //необходимо сделать обход по всем элементам
        //var isCorrectRegExp = regex.test(event.target.value);
        regex = convert_to_regexp(regexp.value);
        toggleMatch(event.target);   
    }

    addition.onclick = function(event){    
        console.log(event);
        var newExample = document.createElement("div");
        var attrClass = document.createAttribute("class");
        attrClass.value = "my-case-block addClass";
        newExample.setAttributeNode(attrClass);
        newExample.innerHTML= '<input type="text" placeholder="Введите пример"><div class="button answer no-matched">NO MATCH</div><div type="button" class="button btn_remove">REMOVE</div>';
        newExample.children[2].onclick = function(event){
            newExample.classList.remove("addClass");
            newExample.classList.add("removeClass");
            newExample.addEventListener("webkitAnimationEnd", function()
            {
                block_cases.removeChild( newExample );
            })
        }
    var block = document.getElementById("block-cases");
    block.appendChild(newExample);
    for(var i = 0; i < block_cases.children.length;i++){
            toggleMatch(block_cases.children[i].children[0]);
        }
    }
}

function toggleMatch(input){
    regex = convert_to_regexp(regexp.value);
    var div = input.nextElementSibling;
    console.log(div);
    var isCorrectRegExp = regex.test(input.value);
    if(isCorrectRegExp){ //если выражение подходит по регулярку
        div.classList.remove("no-matched");
        div.classList.add("matched");
        div.innerHTML = "MATCH";
    }
    else{
        div.classList.remove("matched");
        div.classList.add("no-matched");
        div.innerHTML = "NO MATCH";
    }
}

